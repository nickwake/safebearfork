Feature: searching for elephants

  Rules
  * the search must work for 'elephant' and 'elephants'
  * the search must take you to a page about elephants

  @HighRisk
  @HighImpact
  Scenario Outline: the search bar takes you to elephants when searched for
    When they search for '<searchValue>'
    Then the page is returned with '<title>'
    Examples:
      | searchValue | title |
      | elephant    | Elephant |
      | elephants   | Elephant |