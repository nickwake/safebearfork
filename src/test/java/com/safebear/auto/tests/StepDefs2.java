package com.safebear.auto.tests;

import com.safebear.auto.tests.pages.Elephant;
import com.safebear.auto.tests.pages.Wiki;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class StepDefs2 {
    WebDriver driver = Utils.getDriver();

    Wiki wiki = new Wiki(driver);
    Elephant elephant = new Elephant(driver);

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @When("they search for {string}")
    public void they_search_for(String searchValue) {

        driver.get(Utils.getURL());

        switch (searchValue){
            case "elephant":
                wiki.enterSearch("elephant");
                break;
            case "elephants":
                wiki.enterSearch("elephants");
                break;
            default:
                Assert.fail("The test data is wrong - the only values that can be accepted are 'elephant' or 'elephants'. You entered: " + searchValue);
                break;
        }

        //wiki.enterSearch(searchValue);
        //wiki.clickSearchButton(searchValue);
    }

    @Then("the page is returned with {string}")
    public void the_page_is_returned_with(String title){
       switch (title){
            case "Title is correct":
            Assert.assertTrue(elephant.findElephant().contains(title));
            break;
            case "Title is also correct":
                Assert.assertTrue(elephant.findElephant().contains(title));
                break;
        }
        //elephant.findElephant().contains(title);
    }
}
