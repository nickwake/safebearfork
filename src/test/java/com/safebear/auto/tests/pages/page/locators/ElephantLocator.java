package com.safebear.auto.tests.pages.page.locators;

import lombok.Data;
import org.openqa.selenium.By;
@Data
public class ElephantLocator {
    // title = .//h1[contains(.,"Elephant")]
    private By title = By.xpath(".//h1[contains(.,'Elephant')]");

}
