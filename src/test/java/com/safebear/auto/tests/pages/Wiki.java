package com.safebear.auto.tests.pages;


import com.safebear.auto.tests.pages.page.locators.WikiLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class Wiki {

    WikiLocators wikiLocators = new WikiLocators();

    @NonNull
    WebDriver driver;

    public void enterSearch (String searchValue){
        driver.findElement(wikiLocators.getSearchBar()).sendKeys(searchValue);
    }

    public void clickSearchButton(String searchButton) {
        driver.findElement(wikiLocators.getSearchButton()).click();
    }
}