package com.safebear.auto.tests.pages;

import com.safebear.auto.tests.pages.page.locators.ElephantLocator;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class Elephant {

    ElephantLocator elephantLocator = new ElephantLocator();

    @NonNull
    WebDriver driver;

    public String findElephant (){
        return driver.findElement(elephantLocator.getTitle()).getText();
    }
}