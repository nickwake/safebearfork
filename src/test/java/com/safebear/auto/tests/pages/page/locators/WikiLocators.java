package com.safebear.auto.tests.pages.page.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class WikiLocators {
    // search bar: .//input[@type="search"]
    // search button: .//input[@name="go"]
    private By searchBar = By.xpath(".//input[@type='search']");

    private By searchButton = By.xpath(".//input[@name='go']");

}
