package com.safebear.auto.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        // this is where my html report will be
        plugin = {"pretty", "html:target/cucumber"},
        // this is changing to 'not @to-do' in future
        tags = "not @tag",
        // this is where the tests will be stored
        glue = "com.safebear.auto.tests",
        //This is running all of the WikiLocators.features in this folder
        features = "classpath:wiki.features"
)
public class RunCukes extends AbstractTestNGCucumberTests {
}
