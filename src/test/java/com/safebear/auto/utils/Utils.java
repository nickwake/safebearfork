package com.safebear.auto.utils;

// selenuim = webdriver

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    /**
     * private = only for this class
     * static = only one variable
     * final = can't be changed after its value is set
     *
     * The name of the variable is in caps because it never changes
     *
     * System.getProperty looks at our CI (Continuous Integration (Jenkins)) tool and gets the URL for the test environment
     * The default test environment is the 'def' value
     */
    private static final String URL = System.getProperty("url","https://en.wikipedia.org/wiki/Main_Page");
    private static final String BROWSER = System.getProperty("browser","chrome");

    /**
     * This is the getter for the URL
     * @return the URL of my test environment/application
     */
    public static String getURL(){
        return URL;
    }
    public static WebDriver getDriver (){
        //This tells selenium where our chromedriver is
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver");
        //System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");

        //Here we're setting the size of our browser (1366x768)
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito", "window-size-1366,768"); // examples: "start-maximized"

        //here we are returning the driver for our chosen browser
        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver();

            default:
                return new ChromeDriver(options);
        }
    }
    public static String generateScreenShotFileName(){

        //create filename
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())+".png";
    }
    public static void capturescreenshot(WebDriver driver, String fileName) {

        //Take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        //Make sure that the 'screenshots' directory exists
        File file = new File("target/screenshots");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }

            //Copy file to filename and location we set before
            try {
                copy(scrFile, new File("target/screenshots/" + fileName));
                } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    public static WebElement waitForElement(By locator, WebDriver driver){
        //This is where we set the timeout value (of 5 seconds)
        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement element;

        try{
            //Try to find the element straight away
            element = driver.findElement(locator);
        } catch (TimeoutException e){

            //If you cant, use the 'explicit wait'
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            element = driver.findElement(locator);
        } catch (Exception e){

            //If it still isn't there, print a stack trace and take a screenshot
            System.out.println(e.toString());
            // **NEED TO ADD THIS IN AT SOME POINT** capturescreenshot(driver, generateScreenShotFileName());
            throw (e);
        }

        //Return the web element if it's been found
        return element;
    }
}
